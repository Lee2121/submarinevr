﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class SharkAgent : Agent
{
    public SharkMovementAcademy sharkMovementAcademy;

    public const float FORWARD_SPEED = 10.0F;
    public const float YAW_SPEED = 3.0f;
    public const float PITCH_SPEED = 3.0f;
    public const float TARGET_HEIGHT_FROM_BOTTOM = 8.0f;

    public const float TIME_LIMIT_PER_EPISODE = 180.0f;

    private Transform _myTransform;
    private Rigidbody _myRigidbody;
    private BoxCollider _myCollider;

    private float _heightFromBottom;

    private const float PERCEIVE_DIST = 80.0F;

    private Vector3 _previousPosition = Vector3.zero;

    private void Start()
    {
        _myTransform = GetComponent<Transform>();
        _myRigidbody = GetComponent<Rigidbody>();
        _myCollider = GetComponent<BoxCollider>();
    }

    #region Helpers
    Vector3 NormalizePositionWithinPercieveDist(Vector3 position)
    {
        Vector3 positionNormalized;

        positionNormalized.x = position.x / PERCEIVE_DIST;
        positionNormalized.y = position.y / PERCEIVE_DIST;
        positionNormalized.z = position.z / PERCEIVE_DIST;

        return positionNormalized;
    }

    Vector3 GetNormalizedDirectionToTargetFromAgent()
    {
        return (sharkMovementAcademy.GetTargetPointPosition() - _myTransform.position).normalized;
    }
    #endregion

    #region Observations
    private void DoRaycastForPerceive(Vector3 direction, out float hitDistance)
    {
        RaycastHit hit;
        float raycastResult;
        
        if (Physics.Raycast(_myTransform.position, direction, out hit, PERCEIVE_DIST))
        {
            // If the raycast was successful, the raycastResult is the normalized distance from 0 to PERCEIVE_DIST
            raycastResult = hit.distance / PERCEIVE_DIST;

            AddVectorObs(raycastResult); // Input the normalized distance to what the raycast hit
            AddVectorObs(1f); // Flag that the hit was successful
            hitDistance = hit.distance; // Record the distance of the raycast
        }
        else
        {
            // The raycast did not hit anything
            AddVectorObs(1f); // Flag that the distance is at or above the maximum distance
            AddVectorObs(0f); // Flag that the hit was not successful
            hitDistance = -1; // Flag that there was no distance
        }

        Debug.DrawRay(_myTransform.position, direction * hit.distance);
    }

    // Use raycasts to orient the agent in the world.
    // 16 Observation Vectors (2 per raycast)
    private void PerceiveSurroundings()
    {
        float hitDistance;

        // Look below the agent, and cache off the distance to the hit. Will be used later as a partial reward
        DoRaycastForPerceive(Vector3.down, out hitDistance);
        _heightFromBottom = hitDistance;

        // Look up and forward from the agent
        DoRaycastForPerceive(_myTransform.forward + _myTransform.up, out hitDistance);

        // Look downwards from the agent
        DoRaycastForPerceive(_myTransform.forward - _myTransform.up, out hitDistance);

        // Look 90 degrees to the right of the agent
        DoRaycastForPerceive(_myTransform.right, out hitDistance);

        // Look 45 degrees to the right of the agent
        DoRaycastForPerceive((_myTransform.forward + _myTransform.right).normalized, out hitDistance);

        // Look ahead of the agent
        DoRaycastForPerceive(_myTransform.forward, out hitDistance);

        // Look 45 degrees to the left of the agent
        DoRaycastForPerceive((_myTransform.forward - _myTransform.right).normalized, out hitDistance);

        // Look to the left of the agent
        DoRaycastForPerceive(_myTransform.right * -1, out hitDistance);
    }

    // 28 Observation Vectors (6 position, 6 velocity, 16 raycast)
    public override void CollectObservations()
    {

        // Normalized relative position from the shark agent to the target point
        Vector3 targetPointRelativePosition = sharkMovementAcademy.GetTargetPointTransform().InverseTransformPoint(_myTransform.position);
        AddVectorObs(NormalizePositionWithinPercieveDist(targetPointRelativePosition));

        // Normalized position of the shark agent within the stage
        AddVectorObs(sharkMovementAcademy.GetNormalizedPositionOnStage(_myTransform.position));

        // Normalized distance to the target point
        float normalizedDistance = Vector3.Distance(_myTransform.position, sharkMovementAcademy.GetTargetPointPosition());
        normalizedDistance /= PERCEIVE_DIST;
        AddVectorObs(normalizedDistance);

        // Current velocity of the shark
        AddVectorObs(_myRigidbody.velocity.normalized);

        // Current angular velocity of the shark
        AddVectorObs(_myRigidbody.angularVelocity.normalized);

        // How close is the shark pointing to the target
        AddVectorObs(Vector3.Dot(GetNormalizedDirectionToTargetFromAgent(), _myTransform.forward));

        // Degree to which the shark is moving towards the target
        AddVectorObs(Vector3.Dot(GetNormalizedDirectionToTargetFromAgent(), _myRigidbody.velocity.normalized));

        // Array of sensors from the shark
        PerceiveSurroundings();
    }
    #endregion

    #region Player Control
    public void Update()
    {
        //UpdatePlayerAction();
    }

    private void UpdatePlayerAction()
    {
        // Pitch
        if(Input.GetKey(KeyCode.W))
        {
            _myRigidbody.AddTorque(_myTransform.right * PITCH_SPEED);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            _myRigidbody.AddTorque(-_myTransform.right * PITCH_SPEED);
        }

        // Yaw
        if(Input.GetKey(KeyCode.A))
        {
            _myRigidbody.AddTorque(-_myTransform.up * YAW_SPEED);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _myRigidbody.AddTorque(_myTransform.up * YAW_SPEED);
        }

        // Forward Speed
        if(Input.GetKey(KeyCode.Space))
        {
            _myRigidbody.AddForce(_myTransform.forward * FORWARD_SPEED);
        }
    }
    #endregion

    #region Agent Actions and Rewards
    private void UpdateAgentManueverForces(float[] vectorAction)
    { 
        float forwardForce = Mathf.Clamp(vectorAction[0], 0.5f, 1); // Forward Force
        float yawForce = vectorAction[1]; // Yaw Angular Force
        float pitchForce = vectorAction[2]; // Pitch Angular Force

        _myRigidbody.AddForce(_myTransform.forward * forwardForce * FORWARD_SPEED);
        _myRigidbody.AddTorque(_myTransform.up * yawForce * YAW_SPEED);
        _myRigidbody.AddTorque(_myTransform.right * pitchForce * PITCH_SPEED);
    }

    private void UpdateAgentPartialRewards()
    {
        // Reward the agent for moving towards the target
        if (Vector3.Dot(GetNormalizedDirectionToTargetFromAgent(), _myTransform.forward) > sharkMovementAcademy._directionalRewardThreshold)
        {
            Debug.Log("moving towards");
            AddReward(0.1f);
        }
    }
    
    public override void AgentAction(float[] vectorAction, string textAction)
    {
        // Apply forces to the agent
        UpdateAgentManueverForces(vectorAction);

        // Reward the agent for getting closer or reaching the target
        //UpdateAgentPartialRewards();
    }
    #endregion

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log( _myTransform.gameObject.name + " hit " + collision.gameObject.name + " - Giving punishment");
        AddReward(-1.0f);

        // Reset the agent so that it can try again
        Done();
    }

    private void OnTriggerEnter(Collider other)
    {
        // If the agent collides with the target point, give them a reward
        if (other.gameObject.tag == "AgentTargetPoint")
        {
            Debug.Log("Agent reached the target - Giving reward");
            SetReward(1.0f);
            Done();
        }
    }

    public override void AgentReset()
    {
        // Get a random position to place the shark at
        _myTransform.position = sharkMovementAcademy.GetResetPositionForSharkAgent();

        // Face the agent towards the target
        _myTransform.LookAt(sharkMovementAcademy.GetTargetPointPosition());

        // Rotate the agent within the range defined by the academy
        float respawnRotationAngleX = Random.Range(-sharkMovementAcademy._respawnRotationRange, sharkMovementAcademy._respawnRotationRange);
        float respawnRotationAngleY = Random.Range(-sharkMovementAcademy._respawnRotationRange, sharkMovementAcademy._respawnRotationRange);
        float respawnRotationAngleZ = Random.Range(-sharkMovementAcademy._respawnRotationRange, sharkMovementAcademy._respawnRotationRange);
        _myTransform.Rotate(respawnRotationAngleX, respawnRotationAngleY, respawnRotationAngleZ);

        // Zero out the rigidbody forces
        _myRigidbody.velocity = Vector3.zero;
        _myRigidbody.angularVelocity = Vector3.zero;
    }
}
