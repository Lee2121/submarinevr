﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class SharkMovementAcademy : Academy
{
    public GameObject targetPointPrefab;
    private GameObject targetPoint;

    public const float STAGE_X_MIN = 0;
    public const float STAGE_X_MAX = 400;
    public const float STAGE_Y_MIN = 0;
    public const float STAGE_Y_MAX = 400;
    public const float STAGE_Z_MIN = 0;
    public const float STAGE_Z_MAX = 400;

    public float _targetSize { get; private set; }
    public float _respawnRadiusFromTarget { get; private set; }
    public float _directionalRewardThreshold { get; private set; }
    public int _respawnRotationRange { get; private set; }

    public override void AcademyReset()
    {
        _targetSize = resetParameters["TargetSize"];
        _respawnRadiusFromTarget = resetParameters["RespawnRadiusFromTarget"];
        _directionalRewardThreshold = resetParameters["DirectionalRewardThreshold"];
        _respawnRotationRange = (int)resetParameters["RespawnRotationRange"];

        // Scale the target
        if(targetPoint)
        {
            targetPoint.transform.localScale = new Vector3(_targetSize, _targetSize, _targetSize);
        }
    }

    public static Vector3 GetRandomPositionOnStageOnCircle(float fCircleRadius)
    {
        Vector3 returnCoord = Vector3.zero;

        // Get a random angle to place the coord at
        float angle = Random.value * 360.0f;

        returnCoord.x = STAGE_X_MAX/2 + fCircleRadius * Mathf.Sin(angle * Mathf.Deg2Rad);
        returnCoord.z = STAGE_Z_MAX/2 + fCircleRadius * Mathf.Cos(angle * Mathf.Deg2Rad);

        returnCoord.y = GenericUtility.GetGroundYAtCoord(returnCoord);

        return returnCoord;
    }

    public static Vector3 GetRandomPositionOnStage(float fOffsetFromSides = 0.0f)
    {
        Vector3 returnCoord = Vector3.zero;

        returnCoord.x = Random.Range(STAGE_X_MIN + fOffsetFromSides, STAGE_X_MAX - fOffsetFromSides);
        returnCoord.z = Random.Range(STAGE_Z_MIN + fOffsetFromSides, STAGE_Z_MAX - fOffsetFromSides);

        returnCoord.y = GenericUtility.GetGroundYAtCoord(returnCoord);

        return returnCoord;
    }

    public Vector3 GetResetPositionForSharkAgent()
    {
        Vector3 returnCoord = GetRandomPositionOnStageOnCircle(_respawnRadiusFromTarget);

        // Offset the found position to make sure that it is hovering off the ground rather than clipping through it
        returnCoord.y += 25.0f;

        return returnCoord;
    }

    private void Start()
    {
        Vector3 targetStartPosition = new Vector3(STAGE_X_MAX / 2, 0, STAGE_Z_MAX / 2); //GetRandomPositionOnStage( 20.0f );

        targetStartPosition.y += 20.0f;

        targetPoint = Instantiate<GameObject>(targetPointPrefab, targetStartPosition, Quaternion.identity);
    }

    public Vector3 GetTargetPointPosition()
    {
        return targetPoint.transform.position;
    }

    public Transform GetTargetPointTransform()
    {
        return targetPoint.transform;
    }

    /// <summary>
    /// Normalizes the passed in position to the bounds of the stage. Used to prepare data to be used for observation for machine learning
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public Vector3 GetNormalizedPositionOnStage(Vector3 position)
    {
        Vector3 positionNormalized;

        positionNormalized.x = (position.x - STAGE_X_MIN) / (STAGE_X_MAX - STAGE_X_MIN);
        positionNormalized.y = (position.y - STAGE_Y_MIN) / (STAGE_Y_MAX - STAGE_Y_MIN);
        positionNormalized.z = (position.z - STAGE_Z_MIN) / (STAGE_Z_MAX - STAGE_Z_MIN);

        return positionNormalized;
    }
}
