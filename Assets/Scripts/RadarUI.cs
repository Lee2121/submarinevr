﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadarUI : Singleton<RadarUI> {

    protected RadarUI() { } // guarentee this will always be a singleton only - can't use the constructor!

    public GameObject[] blipableObjects;
    public GameObject blipParent;

    public float fMaxBlipDistance = 200.0f;

    [SerializeField]
    private GameObject radarBlipPrefab;

    private GameObject[] radarBlips;
    private RectTransform radarCanvasRect;

    public float fPulseInterval = 3.0f;
    public float fBlipFadeOutTime = 1.0f;
    private float fTimeSinceLastPulse;

    // Use this for initialization
    void Start()
    {
        radarCanvasRect = blipParent.GetComponent<RectTransform>();

        radarBlips = new GameObject[blipableObjects.Length];
    }

    /// <summary>
    /// Instantiates a blip on the radar. 
    /// </summary>
    /// <param name="iBlipableObject"></param>
    void CreateBlipForObject( int iBlipableObject )
    {
        Debug.Log("Creating a blip for object " + blipableObjects[iBlipableObject].name);
        radarBlips[iBlipableObject] = Instantiate(radarBlipPrefab);

        // Set the created blip to have a parent of the radar screen
        radarBlips[iBlipableObject].transform.SetParent(blipParent.transform, false);
    }

    /// <summary>
    /// Destroys the desired blip on the radar
    /// </summary>
    /// <param name="iBlipableObject"></param>
    void DestroyBlipForObject(int iBlipableObject)
    {
        if (radarBlips[iBlipableObject])
        {
            Debug.Log("Destroying blip for object " + blipableObjects[iBlipableObject].name);
            Destroy(radarBlips[iBlipableObject]);
        }
    }

    void UpdateBlipPosition( int iBlipableObject)
    {
        Vector2 vMapPosition = Vector2.zero;

        // Position the blip. Get the position of the the blipable object relative to the player.
        Vector3 vRelativePosition = Globals.Instance.PlayerShip.transform.InverseTransformPoint(blipableObjects[iBlipableObject].transform.position);

        // Get the map position by divinding our relative position by the max blipable distance
        float fWidthPercentage = vRelativePosition.x / fMaxBlipDistance;
        float fHeigntPercentage = vRelativePosition.z / fMaxBlipDistance;

        vMapPosition.x = fWidthPercentage * (radarCanvasRect.rect.width / 2); // Divde by 2 because the center of the UI is 0, 0
        vMapPosition.y = fHeigntPercentage * (radarCanvasRect.rect.height / 2);

        // Apply the blip position
        radarBlips[iBlipableObject].GetComponent<RectTransform>().localPosition = vMapPosition;
    }

    void UpdateBlipAlpha( int iBlipableObject )
    {
        Image blipImage = radarBlips[iBlipableObject].GetComponent<Image>();

        Color blipColor = blipImage.color;

        blipColor.a = Mathf.Lerp(1, 0, fTimeSinceLastPulse / fBlipFadeOutTime);

        blipImage.color = blipColor;
    }

    /// <summary>
    /// Positions, scales, fades, etc. the blip for the desired blipable object
    /// </summary>
    /// <param name="iBlipableObject"></param>
    void UpdateBlipForBlipableObject( int iBlipableObject )
    {
        UpdateBlipPosition(iBlipableObject);
        UpdateBlipAlpha(iBlipableObject);
    }

    void UpdatePulseTimer()
    {
        fTimeSinceLastPulse += Time.deltaTime;

        if(fTimeSinceLastPulse >= fPulseInterval)
        {
            // Play the sonar audio and reset the timer
            SoundManager.Instance.PlayOneShot(SoundManager.OneShot.Sonar_Ping);
            fTimeSinceLastPulse = 0;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePulseTimer();

        for(int iBlipableObject = 0; iBlipableObject < blipableObjects.Length; iBlipableObject++)
        {
            // Is the entity within range
            float fDist = Vector3.Distance(Globals.Instance.PlayerShip.transform.position, blipableObjects[iBlipableObject].transform.position);

            if(fDist > fMaxBlipDistance)
            {
                // Out of blip range, cleanup our blip
                DestroyBlipForObject(iBlipableObject);
            }
            else
            {
                // We're within blipping range. Create a blip if it doesn't already exist
                if(!radarBlips[iBlipableObject])
                {
                    // Instantiate the blip on the radar
                    CreateBlipForObject(iBlipableObject);
                }

                // Update the blip
                UpdateBlipForBlipableObject(iBlipableObject);
            }
        }
	}
}
