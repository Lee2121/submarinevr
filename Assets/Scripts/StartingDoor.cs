﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{ 
    public Vector3 _closedRotation;
    public Vector3 _openRotation;

    public float _transitionSpeed = 3.0f;
    
    public enum DoorState
    {
        Closed,
        Closing,
        Opening, 
        Open
    }

    private DoorState _doorState;

	// Use this for initialization
	void Start ()
    {
        // TODO: Make the player play a short minigame to open the door
        _doorState = DoorState.Opening;
	}
	
    /// <summary>
    /// Handles moving the door to the specified state
    /// </summary>
    /// <param name="eState">The state to move the door to</param>
    public void GoToDoorState(DoorState eState)
    {
        if(_doorState != eState)
        {
            Debug.Log("Door is moving from state " + _doorState + " to " + eState);
            _doorState = eState;
        }
    }
    
    private void UpdateOpeningDoor()
    {
        float step = _transitionSpeed * Time.deltaTime;

        float doorAngleX = Mathf.LerpAngle(transform.eulerAngles.x, _openRotation.x, step);
        float doorAngleY = Mathf.LerpAngle(transform.eulerAngles.y, _openRotation.y, step);
        float doorAngleZ = Mathf.LerpAngle(transform.eulerAngles.z, _openRotation.z, step);

        transform.eulerAngles = new Vector3(doorAngleX, doorAngleY, doorAngleZ);
        
        // Once the timer is done, move to the opened state
        if(doorAngleX == _openRotation.x && doorAngleY == _openRotation.y && doorAngleZ == _openRotation.z)
        {
            GoToDoorState(DoorState.Open);
        }
    }

	// Update is called once per frame
	void Update ()
    {

        switch (_doorState)
        {
            case DoorState.Closed:
                // nothing yet
                break;

            case DoorState.Closing:
                // Nothing yet
                break;

            case DoorState.Open:
                // Nothing yet
                break;

            case DoorState.Opening:

                UpdateOpeningDoor();
                break;
        }
    }
}
