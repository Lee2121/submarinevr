﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class PipelineScript : MonoBehaviour
{
    public GameObject pipelinePrefab;
    public GameObject pipelineJointPrefab;

    private List<GameObject> pipelineJoints;
    private List<GameObject> pipelinePipes;

    private bool bInitialized;

    private bool IsPipelineJointIndexValid(int iPipelineJoint)
    {
        // Validate the given pipeline connections
        if (iPipelineJoint > pipelineJoints.Count || iPipelineJoint < 0)
        {
            return false;
        }

        return true;
    }

    private Vector3 GetPositionForNewPipelinePoint()
    {
        // Get the editor camera
        Vector3 vEditorPosition = SceneView.lastActiveSceneView.camera.transform.position;
        Vector3 vEditorDirection = SceneView.lastActiveSceneView.camera.transform.rotation * Vector3.forward;

        Debug.DrawRay(vEditorPosition, vEditorDirection);

        // Raycast from the center of the screen to terrain. If no collision point is found, just create it at the camera's position
        RaycastHit hit;
        if (Physics.Raycast(vEditorPosition, vEditorDirection, out hit, 5000))
        {
            return hit.point;
        }
        else
        {
            return vEditorPosition;
        }
    }

    private Vector3 GetMidpointBetweenPipelineConnections(int iConnection1, int iConnection2)
    {
        GameObject pipelineConnection1, pipelineConnection2;

        if(!IsPipelineJointIndexValid(iConnection1))
        {
            Debug.LogError("GetMidpointBetweenPipelineConnections - Connection1 " + iConnection1 + " is not a valid connection. There are only " + (pipelineJoints.Count) + " connections.");
            return Vector3.zero;
        }

        if(!IsPipelineJointIndexValid(iConnection2))
        {
            Debug.LogError("GetMidpointBetweenPipelineConnections - Connection2 " + iConnection2 + " is not a valid connection. There are only " + (pipelineJoints.Count) + " connections.");
            return Vector3.zero;
        }

        // Make sure the connections exist
        pipelineConnection1 = pipelineJoints[iConnection1];
        pipelineConnection2 = pipelineJoints[iConnection2];

        if(!pipelineConnection1)
        {
            Debug.LogError("GetMidpointBetweenPipelineConnections - Connection1 " + iConnection1 + " is not an existing connection object.");
            return Vector3.zero;
        }

        if (!pipelineConnection2)
        {
            Debug.LogError("GetMidpointBetweenPipelineConnections - Connection2 " + iConnection2 + " is not an existing connection object.");
            return Vector3.zero;
        }

        // Determine the midpoint
        Vector3 returnVector;
        returnVector.x = (pipelineConnection1.transform.position.x + pipelineConnection2.transform.position.x) / 2;
        returnVector.y = (pipelineConnection1.transform.position.y + pipelineConnection2.transform.position.y) / 2;
        returnVector.z = (pipelineConnection1.transform.position.z + pipelineConnection2.transform.position.z) / 2;

        return returnVector;
    }

    private void UpdatePipelineData(bool bClearAllData = false)
    {

        int i;
        int iNumPipelineJoints = pipelineJoints.Count;

        for(i = pipelineJoints.Count - 1; i >= 0; i--)
        {
            if(bClearAllData && pipelineJoints[i])
            {
                DestroyImmediate(pipelineJoints[i]);
            }

            if (pipelineJoints[i] == null)
            {
                pipelineJoints.RemoveAt(i);
            }
        }

        for(i = pipelinePipes.Count - 1; i >= 0; i--)
        {
            if(bClearAllData && pipelinePipes[i])
            {
                DestroyImmediate(pipelinePipes[i]);
            }

            if (pipelinePipes[i] == null)
            {
                pipelinePipes.RemoveAt(i);
            }
        }

        pipelineJoints.TrimExcess();
        pipelinePipes.TrimExcess();
    }

    /// <summary>
    /// Rotates and scales the passed in pipe transform so that it faces and connects with the designated joint
    /// </summary>
    /// <param name="pipeTransform"></param>
    /// <param name="iJoint"></param>
    private void OrientPipelineForJoint(Transform pipeTransform, int iJoint)
    {
        // Validity checks
        if(!IsPipelineJointIndexValid(iJoint))
        {
            Debug.LogError("Pipeline joint " + iJoint + " is not valid.");
            return;
        }

        // Fixes a weird issue where the rotation of the pipe was causing issues when using Quaternion.FromToRotation. Kinda looked like gimbal lock (would cause the pipes to be rotated straight up).
        pipeTransform.rotation = Quaternion.identity;

        // Rotate the pipeline so that it faces the next pipeline joint
        Vector3 vDirection = pipeTransform.localPosition - pipelineJoints[iJoint].transform.localPosition;
        Quaternion toRotation = Quaternion.FromToRotation(pipeTransform.up, vDirection);
        pipeTransform.localRotation = toRotation;

        // Scale the pipeline so that it connects to its two joints
        float fDistance = Vector3.Distance(pipelineJoints[iJoint].transform.position, pipeTransform.position);
        pipeTransform.localScale = new Vector3(pipeTransform.localScale.x, fDistance, pipeTransform.localScale.z);
    }

    public void AddPipelinePoint()
    {
        // Make sure that we have valid data.
        if(!bInitialized)
        {
            pipelineJoints = new List<GameObject>();
            pipelinePipes = new List<GameObject>();
            bInitialized = true;
            Debug.Log("Initialized new pipeline data.");
        }
        else
        {
            UpdatePipelineData();
        }

        Vector3 vPipelineJointPosition = GetPositionForNewPipelinePoint();

        int iPipelineNum = pipelineJoints.Count;
 
        // Create the pipeline joint at the new point
        GameObject newPipelineJoint = Instantiate<GameObject>(pipelineJointPrefab, vPipelineJointPosition, Quaternion.identity);

        newPipelineJoint.name = "PipelineJoint" + iPipelineNum;

        // Set the pipeline to be a child of the gameobject this script is running from (should be an empty game object to hold all pipeline objects)
        newPipelineJoint.transform.SetParent(this.transform);
        
        // Add the pipeline joint to the list 
        pipelineJoints.Add(newPipelineJoint);
        Debug.Log("Created pipeline joint " + iPipelineNum);

        // If we have already created our first pipeline, we can attach the first and second ones with a connection pipe
        if(iPipelineNum > 0)
        {
            // Get the halfway point between the previous point and the new point
            Vector3 vNewPipelinePos = GetMidpointBetweenPipelineConnections(iPipelineNum - 1, iPipelineNum);
            
            // Create the pipeline itself
            GameObject newPipeline = Instantiate<GameObject>(pipelinePrefab, vNewPipelinePos, Quaternion.identity, this.transform);

            // Name the pipeline
            newPipeline.name = "Pipeline" + (iPipelineNum - 1) + "to" + iPipelineNum;

            // Orient the new pipeline joint
            OrientPipelineForJoint(newPipeline.transform, iPipelineNum);

            // Add the pipeline joint to the list
            pipelinePipes.Add(newPipeline);

            Debug.Log("Created pipeline pipe connecting pipeline joint " + (iPipelineNum - 1) + " and pipeline joint " + iPipelineNum);
        }
    }

    public void RemoveLastJoint()
    {
        int iPipelineNum = pipelineJoints.Count - 1;

        if(IsPipelineJointIndexValid(iPipelineNum))
        {
            if (pipelineJoints[iPipelineNum])
            {
                DestroyImmediate(pipelineJoints[iPipelineNum]);
                pipelineJoints.RemoveAt(iPipelineNum);
            }

            if (pipelinePipes[iPipelineNum - 1]) // There is one less pipe than pipeline joints (because we need 2 joints to create a pipe)
            {
                DestroyImmediate(pipelinePipes[iPipelineNum - 1]);
                pipelinePipes.RemoveAt(iPipelineNum - 1);
            }
        }
    }

    /// <summary>
    /// Loop through all points in the path and make sure that the pipeline pipes are oriented correctly
    /// </summary>
    public void UpdateAllPipelinePoints()
    {
        // Make sure that all the pipeline data is up to date before running
        UpdatePipelineData();

        int iJoint, iPipe;

        // Starts at joint 1 as there isn't a pipe for joint 0
        for (iJoint = 1; iJoint < pipelineJoints.Count; iJoint++)
        {
            // Get the corresponding pipe index for the joint
            iPipe = iJoint - 1; // -1 beacuse there is one less pipe than there is joints

            Debug.Log("Updating joint " + iJoint + " and pipe " + iPipe);

            // Position the pipe midway in between the two joints
            pipelinePipes[iPipe].transform.position = GetMidpointBetweenPipelineConnections(iJoint - 1, iJoint);

            // Orient the pipeline
            OrientPipelineForJoint(pipelinePipes[iPipe].transform, iJoint);
        }
    }

    public void ClearAllPipelinePoints()
    {
        UpdatePipelineData(true);
    }
}

#endif
