﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PipelineScript))]
public class PipelineEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PipelineScript pipelineScript = (PipelineScript)target;
        if (GUILayout.Button("Add Point"))
        {
            pipelineScript.AddPipelinePoint();
        }

        if(GUILayout.Button("Remove Last Point"))
        {
            pipelineScript.RemoveLastJoint();
        }

        if(GUILayout.Button("Update All Points"))
        {
            pipelineScript.UpdateAllPipelinePoints();
        }

        if(GUILayout.Button("Clear All Points"))
        {
            pipelineScript.ClearAllPipelinePoints();
        }
    }
}

#endif