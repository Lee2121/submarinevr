﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipelineJoint : MonoBehaviour
{

    private PipelineJoint iIncomingConnection;
    public PipelineJoint GetIncomingConnection() { return iIncomingConnection; }

    private PipelineJoint iOutgoingConnection;
    public PipelineJoint GetOutGoingConnection() { return iOutgoingConnection; }

    private int iJointIndex = -1;
    public int GetJointIndex() { return iJointIndex; }

    private GameObject goConnectingPipe;

    void InitializePipelineJoint( int iNewJointIndex )
    {
        iJointIndex = iNewJointIndex;


    }

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
