﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorqueUpright : MonoBehaviour
{

    private Rigidbody thisRigidbody;

    public float fUprightTorque = 100.0f;

	// Use this for initialization
	void Start ()
    {
        thisRigidbody = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Quaternion rotation = Quaternion.FromToRotation(thisRigidbody.transform.up, Vector3.up);

        Vector3 rotationAngles = new Vector3(rotation.x, rotation.y, rotation.z);

        thisRigidbody.AddTorque(rotationAngles * fUprightTorque * Time.deltaTime);
    }
}
