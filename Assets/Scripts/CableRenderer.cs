﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableRenderer : MonoBehaviour
{
    // Bouyancy forces
    public const float BOUYANCY_FORCE_Y = 0.1f;
    private Vector3 _bouyancyForce = new Vector3(0.0f, BOUYANCY_FORCE_Y, 0.0f);

    // Start and end points of the cable
    public Transform _cableStartPoint;
    public Transform _cableEndPoint;

    // Line renderer
    private LineRenderer _lineRenderer;

    // Stretch Iterations
    public const int MAX_NUM_STRETCH_ITERATIONS = 1;

    #region Cable Sections
    public struct CableSection
    {
        public Vector3 _curPos;
        public Vector3 _prevPos;

        // To write RopeSection.zero
        public static readonly CableSection zero = new CableSection(Vector3.zero);

        // Constructor
        public CableSection(Vector3 pos)
        {
            _curPos = pos;
            _prevPos = pos;
        }
    }

    // List of all the individual sections of cable
    private List<CableSection> _allCableSections = new List<CableSection>();

    // Length of each cable segment
    private const float CABLE_SECTION_LENGTH = 0.5f;
    private const float CABLE_SECTION_WIDTH = 0.2f;
    private const float NUM_CABLE_SECTIONS = 15;
    #endregion

    private void InitializeCableData()
    {
        Vector3 cableSectionPos = _cableStartPoint.position;

        // Loops through all cable sections, and initializes their position based on the given start point
        for(int iCableSection = 0; iCableSection < 15; iCableSection++)
        {
            // Add the new cable section to our list.
            _allCableSections.Add(new CableSection(cableSectionPos));

            // Offset the starting point of our next section of cable by the length of each segment
            cableSectionPos.y += CABLE_SECTION_LENGTH;
        }
    }

    // Use this for initialization
    void Start ()
    {
        // Initialize the line renderer for the cable
        _lineRenderer = GetComponent<LineRenderer>();

        // Initialize the cable data
        InitializeCableData();
    }

    private void DisplayCable()
    {
        // Set the renderer's width
        _lineRenderer.startWidth = CABLE_SECTION_WIDTH;
        _lineRenderer.endWidth = CABLE_SECTION_WIDTH;

        // Populate an array of positions with the current positions of all the cable sections
        Vector3[] cableSections = new Vector3[_allCableSections.Count];

        for (int iCableSection = 0; iCableSection < cableSections.Length; iCableSection++)
        {
            cableSections[iCableSection] = _allCableSections[iCableSection]._curPos;
        }

        // Set the line renderer positions with our array of cable segment positions
        _lineRenderer.positionCount = cableSections.Length;
        _lineRenderer.SetPositions(cableSections);
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Update the cable's line renderer
        DisplayCable();

        // Keep the gameobject at the end of the cable attached
        _cableEndPoint.position = _allCableSections[_allCableSections.Count - 1]._curPos;
        _cableEndPoint.LookAt(_allCableSections[_allCableSections.Count - 2]._curPos);
	}

    private void EnforceMaximumCableStretch()
    {
        for(int iCableSection = 0; iCableSection < _allCableSections.Count - 1; iCableSection++)
        {
            CableSection currentCableSection = _allCableSections[iCableSection];
            CableSection nextCableSection = _allCableSections[iCableSection + 1];

            // Get the distance between the sections
            float dist = (currentCableSection._curPos - nextCableSection._curPos).magnitude;

            // Get the stretch or compression
            float adjustmentMagnitude = Mathf.Abs(dist - CABLE_SECTION_LENGTH);

            Vector3 adjustmentDirection = Vector3.zero;

            // Distance is greater, need to compress this section
            if(dist > CABLE_SECTION_LENGTH)
            {
                adjustmentDirection = (currentCableSection._curPos - nextCableSection._curPos).normalized;
            }
            // Distance is less, need to stretch this section
            else if(dist < CABLE_SECTION_LENGTH)
            {
                adjustmentDirection = (nextCableSection._curPos - currentCableSection._curPos).normalized;
            }
            
            if(iCableSection > 0)
            {
                // To make the stretch/compression adjustment, we move both the current and the next section towards each other. 
                // Each section needs to move half the distance, which totals the entire intended adjustment.
                Vector3 halfAdjustment = (adjustmentDirection * adjustmentMagnitude) * 0.5f;

                // Apply the half adjustment to the next cable section, to move it towards the current cable
                nextCableSection._curPos += halfAdjustment;
                _allCableSections[iCableSection + 1] = nextCableSection;

                // Apply the half adjustment to the current cable section, to move it towards the next one.
                currentCableSection._curPos -= halfAdjustment;
                _allCableSections[iCableSection] = currentCableSection;
            }
            else
            {
                // Since the cable is connected to something at the [0] index, only move the next cable section the full adjustment amount
                Vector3 adjustment = adjustmentDirection * adjustmentMagnitude;

                // Apply the full adjustment to the next cable section
                nextCableSection._curPos += adjustment;
                _allCableSections[iCableSection + 1] = nextCableSection;
            }
        }
    }

    private void UpdateCableSimulation()
    {
        // Move the first section of the cable to what the cable is attached to
        CableSection currentCableSection = _allCableSections[0];
        currentCableSection._curPos = _cableStartPoint.position;

        // Apply the change to the first cable section
        _allCableSections[0] = currentCableSection;

        // Move the subsequent cable sections in acordance with Verlet Integration
        for (int iCableSection = 1; iCableSection < _allCableSections.Count; iCableSection++)
        {
            currentCableSection = _allCableSections[iCableSection];

            // Calculate the velocity of this section for this update
            Vector3 sectionVelocity = currentCableSection._curPos - currentCableSection._prevPos;

            // Set the previous cable section to be the current section
            currentCableSection._prevPos = currentCableSection._curPos;

            // Find the new current position
            currentCableSection._curPos += sectionVelocity;

            // Add our bouyancy force
            currentCableSection._curPos += _bouyancyForce * Time.fixedDeltaTime;

            // Add it back into the array
            _allCableSections[iCableSection] = currentCableSection;
        }

        for (int iStretchIteration = 0; iStretchIteration < MAX_NUM_STRETCH_ITERATIONS; iStretchIteration++)
        {
            EnforceMaximumCableStretch();
        }
    }

    private void FixedUpdate()
    {
        UpdateCableSimulation();
    }
}