﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DockingPad : MonoBehaviour
{

    public Light[] lights;
    public float fPulseSpeed = 5.0f;

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        // The current percentage that the smoothstep should be at. Ping pongs back and forth at the rate defined by fPulseSpeed
        float t = Mathf.PingPong(Time.time / fPulseSpeed, 1);
        
        for (int i = 0; i < lights.Length; i++)
        {
            lights[i].intensity = Mathf.SmoothStep(0, 1, t);
        }
	}
}
