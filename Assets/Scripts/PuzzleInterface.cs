﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleInterface : MonoBehaviour
{
    private const float INTERFACE_RADIUS = 4.0f;

    #region Docking Position Consts
    private const float DOCKING_POSITION_OFFSET_X = 0.0f;
    private const float DOCKING_POSITION_OFFSET_Y = 0.174f;
    private const float DOCKING_POSITION_OFFSET_Z = 2.304f;
    private const float DOCKING_ROTATION_X = 15.518F;
    private const float DOCKING_ROTATION_Y = 180.0f;
    private const float DOCKING_ROTATION_Z = 0.0f;
    #endregion

    //private SphereCollider _nearbyRangeTrigger; // If the player's ship is inside of this, they are nearby and the puzzle elements should begin to update if necessary
    private SphereCollider _dockingRangeTrigger; // If the player's ship is inside of this, then they are within range to dock to the station
    public GameObject _dockingPoint { get; private set; }

    // List of all the interactable objects linked to this interface
    [SerializeField]
    private InteractablePuzzleObject[] interactableObjects;

	// Use this for initialization
	void Start ()
    {
        // Create a trigger volume around the interface
        _dockingRangeTrigger = this.gameObject.AddComponent<SphereCollider>();
        _dockingRangeTrigger.radius = INTERFACE_RADIUS;
        _dockingRangeTrigger.isTrigger = true;
        _dockingRangeTrigger.tag = "PuzzleInterface";

        // Create a docking point
        _dockingPoint = new GameObject("DockingPoint");
        _dockingPoint.transform.parent = this.transform;
        _dockingPoint.transform.localPosition = new Vector3(DOCKING_POSITION_OFFSET_X, DOCKING_POSITION_OFFSET_Y, DOCKING_POSITION_OFFSET_Z);
        _dockingPoint.transform.localEulerAngles = new Vector3(DOCKING_ROTATION_X, DOCKING_ROTATION_Y, DOCKING_ROTATION_Z);
        _dockingPoint.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }
	
    private bool ShouldInteractablePuzzleObjectsUpdate()
    {
        // If this puzzle interface is not the one the player is nearby, then the puzzle objects should not update
        if(ShipManager.Instance._currentPuzzleInterface != this)
        {
            return false;
        }

        // If the player's ship is extending it's arm, or if the player is currently in control of the arm, update the puzzle objects
        switch(ShipManager.Instance._shipState)
        {
            case ShipManager.ShipState.ExtendingArm:
            case ShipManager.ShipState.DockedAtPuzzle:
                return true;
        }

        return false;
    }

	// Update is called once per frame
	void Update ()
    {
        // If we're set as the current puzzle interface, update the puzzle objects
		if(ShouldInteractablePuzzleObjectsUpdate())
        {
            for(int interactableObject = 0; interactableObject < interactableObjects.Length; interactableObject++)
            {
                // Update our current puzzle object
                interactableObjects[interactableObject].UpdateInteractablePuzzleObject();
            }
        }
	}
}
