﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEngines : Singleton<ShipEngines>
{
    protected ShipEngines() { } // guarentee this will always be a singleton only - can't use the constructor!

    public Rigidbody _myRigidbody { get; private set; }
    public Transform _myTransform { get; private set; }

    public float _currShipSpeed { get; private set; }
    public void KillEnginePower()
    {
        Debug.Log("Killing all engine power.");
        _currShipSpeed = 0;
        _currentThrust = 0;
    }

    #region Thrust
    public float fMaxThrust = 500.0f;
    public float fThrustAcceleration = .25f;

    private float _currentThrust;
    #endregion

    #region Steering
    // Steering CONSTS
    const float MAX_PTICH_INPUT = 30;
    //const float MAX_ROLL_INPUT = 30; roll input is now calculated through yaw input
    const float MAX_YAW_INPUT = 30; 
    const float YAW_TO_ROLL_MODIFIER = 0.5F; // The percentage of the yaw input that will be calculated as roll input

    // Force Modifiers
    public float fRollSpeed = 3.0f;
    public float fPitchSpeed = 5.0f;
    public float fYawSpeed = 3.0f;

    // Read and Adjusted Input
    // Roll, pitch and yaw Input is stored as a value between -1 and 1
    private float _fPitchInput, _fRollInput, _fYawInput;
    public float GetPitchInput() { return _fPitchInput; }
    public float GetRollInput() { return _fRollInput; }
    public float GetYawInput() { return _fYawInput; }
    #endregion

    // Use this for initialization
    private void Start()
    {
        _myRigidbody = this.GetComponent<Rigidbody>();
        _myTransform = this.GetComponent<Transform>();
    }

    public void UpdateThrust()
    {

        // If the player isn't clicking the button, they aren't requesting thrust up/down
        if (!InputManager.Instance.GetControllerClickButton())
        {
            return;
        }

        // Slow Down
        if(InputManager.Instance.GetControllerTouchPos().x < 0.5f)
        {
            _currentThrust -= fThrustAcceleration;
        }
        else // Speed Up
        {
            _currentThrust += fThrustAcceleration;
        }

        // Limit thrust
        _currentThrust = Mathf.Clamp(_currentThrust, 0, fMaxThrust);
    }

    public void UpdateSteering()
    {

        // Read input from the controller
        Vector3 controllerTilt = InputManager.Instance.GetControllerOrientation();

        // Yaw
        _fYawInput = InputManager.Instance.ClampGVRRawInputValue(controllerTilt.x, MAX_YAW_INPUT) / MAX_YAW_INPUT; // Divided by the max to get us the percentage that the axis is currently at. Scale of -1 to 1

        // Roll
        //_fRollInput = ClampGVRRawInputValue(controllerTilt.y, MAX_ROLL_INPUT) / MAX_ROLL_INPUT; // Divided by the max to get us the percentage that the axis is currently at. Scale of -1 to 1

        // New role behavior!!! Take a fraction of the yaw input and apply that here
        _fRollInput = _fYawInput * YAW_TO_ROLL_MODIFIER * -1; // inverted to apply force into the turn

        // Pitch
        _fPitchInput = InputManager.Instance.ClampGVRRawInputValue(controllerTilt.z, MAX_PTICH_INPUT) / MAX_PTICH_INPUT; // Divided by the max to get us the percentage that the axis is currently at. Scale of -1 to 1
    }

    public void UpdateRigidbody()
    {

        // Add the force to the ship
        _myRigidbody.AddForce(_myRigidbody.transform.forward * _currentThrust * Time.deltaTime, ForceMode.Force);

        // Apply roll to the ship
        _myRigidbody.AddTorque(-_myRigidbody.transform.forward * _fRollInput * fRollSpeed * Time.deltaTime);

        // Apply pitch to the ship
        _myRigidbody.AddTorque(-_myRigidbody.transform.right * _fPitchInput * fPitchSpeed * Time.deltaTime);

        // Apply yaw to the ship
        _myRigidbody.AddTorque(-_myRigidbody.transform.up * _fYawInput * fYawSpeed * Time.deltaTime);

        // Update the ship's current speed
        _currShipSpeed = _myRigidbody.velocity.magnitude;
    }
}
