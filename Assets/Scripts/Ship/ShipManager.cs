﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : Singleton<ShipManager>
{
    protected ShipManager() { } // guarentee this will always be a singleton only - can't use the constructor!

    public Rigidbody _myRigidbody { get; private set; }
    public Transform _myTransform { get; private set; }

    #region Ship Main State
    private float _stateTimer = -1;

    public enum ShipState
    {
        PlayerControlled = 0,
        DockingToPuzzle,
        ExtendingArm,
        DockedAtPuzzle,
        StowingArm,
        ExitingDock
    }

    public ShipState _shipState { get; private set; }
    #endregion

    #region Docking
    public const float DOCKING_TRANSITION_TIME = 3.5f;
    public bool _nearDockablePosition { get; private set; }
    public PuzzleInterface _currentPuzzleInterface { get; private set; }
    private Vector3 _dockingTransitionStartPosition;
    private Quaternion _dockingTransitionStartRotation;

    private float _defaultDrag;
    private float _defaultAngularDrag;
    private const float DOCKING_DRAG = 2.0f;
    private const float DOCKING_ANGULAR_DRAG = 3.0f;
    #endregion

    #region State Machine Helpers
    private void SetShipState(ShipState newState)
    {
        if (_shipState != newState)
        {
            Debug.Log("Ship is moving to state " + newState);
            _shipState = newState;
        }
    }
    #endregion

    // Use this for initialization
    void Start ()
    {
        _myRigidbody = this.GetComponent<Rigidbody>();
        _myTransform = this.GetComponent<Transform>();

        _shipState = ShipState.PlayerControlled;

        // Since we're starting in the player controlled state, make sure that the claw is disabled
        ShipArm.Instance.DisableArm();

        _defaultDrag = _myRigidbody.drag;
        _defaultAngularDrag = _myRigidbody.angularDrag;
    }

    #region Main State Machine
    private void UpdateShipPlayerControlled()
    {
        // Update the steering visualizer
        ShipControlStick.Instance.UpdatePlayerPilotinglVisualization();

        // Update the ship engine's
        ShipEngines.Instance.UpdateThrust();
        ShipEngines.Instance.UpdateSteering();
        ShipEngines.Instance.UpdateRigidbody();

        // If we are near a dockable position and the player presses the dock button, start the docking process
        if (_nearDockablePosition && InputManager.Instance.GetControllerAppButtonDown())
        {
            SetShipState(ShipState.DockingToPuzzle);

            // Reset the state timer
            _stateTimer = 0;

            // Set the ship's current input speed to 0
            ShipEngines.Instance.KillEnginePower();

            // Reset docking parameters
            _dockingTransitionStartPosition = Vector3.zero;
            _dockingTransitionStartRotation = Quaternion.identity;
        }
    }

    private void UpdateShipDockingToPuzzle()
    {
        // Update the controller visualizer for docking
        ShipControlStick.Instance.UpdateDockingVisualization();
        
        // Wait until the ship has stopped moving
        if (_myRigidbody.velocity.sqrMagnitude > 0.01f || _myRigidbody.angularVelocity.sqrMagnitude > 0.01f)
        {
            _myRigidbody.drag = DOCKING_DRAG;
            _myRigidbody.angularDrag = DOCKING_ANGULAR_DRAG;
            return;
        }

        // If we haven't initialized the docking state's data, do so now.
        if (_dockingTransitionStartPosition == Vector3.zero || _dockingTransitionStartRotation == Quaternion.identity)
        {
            // cache off the current transform to use when lerping
            _dockingTransitionStartPosition = _myTransform.position;
            _dockingTransitionStartRotation = _myTransform.rotation;

            // reset rigidbody drag values
            _myRigidbody.drag = _defaultDrag;
            _myRigidbody.angularDrag = _defaultAngularDrag;

            // set the rigidbody kinematic
            _myRigidbody.isKinematic = true;
        }

        // Increment the state timer used for lerping
        _stateTimer += Time.deltaTime;

        // Smoothstep our lerp percentage
        float smoothStepPercentage = Mathf.SmoothStep(0, 1, _stateTimer / DOCKING_TRANSITION_TIME);

        // Move the ship to the docking point position
        _myTransform.position = Vector3.Slerp(_dockingTransitionStartPosition, _currentPuzzleInterface._dockingPoint.transform.position, smoothStepPercentage);
        _myTransform.rotation = Quaternion.Slerp(_dockingTransitionStartRotation, _currentPuzzleInterface._dockingPoint.transform.rotation, smoothStepPercentage);

        // Update the timer. When we're finished, move to the docked at puzzle state
        if (_stateTimer >= DOCKING_TRANSITION_TIME)
        {
            SetShipState(ShipState.ExtendingArm);
        }
    }

    private void UpdateShipExtendingArm()
    {
        // Wait until the ship arm finishes moving to it's initial position
        if(ShipArm.Instance.UpdateArmToInitialPosition())
        {
            SetShipState(ShipState.DockedAtPuzzle);
        }
    }

    private void UpdateShipDockedAtPuzzle()
    {
        // Update the player moving the ship arm
        ShipArm.Instance.UpdatePlayerControl();

        // If the player hits the app button, leave the puzzle
        if (_nearDockablePosition && InputManager.Instance.GetControllerAppButtonDown())
        {
            SetShipState(ShipState.StowingArm);
        }
    }

    private void UpdateShipStowingArm()
    {
        if(ShipArm.Instance.UpdateArmToStowedPosition())
        {
            SetShipState(ShipState.ExitingDock);
        }
    }

    private void UpdateShipExitingDock()
    {
        _myRigidbody.isKinematic = false;
        SetShipState(ShipState.PlayerControlled);
    }

    // Update is called once per frame
    void Update()
    {
        switch (_shipState)
        {
            case ShipState.PlayerControlled:
                UpdateShipPlayerControlled();
                break;

            case ShipState.DockingToPuzzle:
                UpdateShipDockingToPuzzle();
                break;

            case ShipState.ExtendingArm:
                UpdateShipExtendingArm();
                break;

            case ShipState.DockedAtPuzzle:
                UpdateShipDockedAtPuzzle();
                break;

            case ShipState.StowingArm:
                UpdateShipStowingArm();
                break;

            case ShipState.ExitingDock:
                UpdateShipExitingDock();
                break;
        }
    }
    #endregion

    #region Trigger Enter and Exit
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PuzzleInterface")
        {
            _nearDockablePosition = true;
            _currentPuzzleInterface = other.GetComponent<PuzzleInterface>();
            Debug.Log("Current docking point is " + other.gameObject.name);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "PuzzleInterface")
        {
            _nearDockablePosition = false;
            _currentPuzzleInterface = other.GetComponent<PuzzleInterface>();
            Debug.Log("Leaving docking point " + other.gameObject.name);
        }
    }
    #endregion
}
