﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControlStick : Singleton<ShipControlStick>
{
    protected ShipControlStick() { } // guarentee this will always be a singleton only - can't use the constructor!
    
    public GameObject controlStickBase;
    public GameObject handle;

    // Public
    public float fDamping = 1.0f;
    public float fControlStickBasePitchOffset = 7.15f; // The control stick at rest has a pitch of this. Use this as an offset for the target rotation

    // Control Stick Base Maximums
    const float MAX_CONTROL_STICK_BASE_PITCH = 15.0f;
    const float MAX_CONTROL_STICK_BASE_ROLL = 15.0f;
    const float MAX_CONTROL_STICK_HANDLE_YAW = 20.0f;

    // Control Handle Maximums

    // Purpose:
    //      This gets us the value that the desired axis should be at to match up 1:1 with our GvrController input. This should be used along with damping to simulate our in game control stick.
    //      When we get input from ShipEngines, it comes as a value between -1 and 1. To use it to lerp, this should be a value between 0 and 1. 
    // Inputs:
    //      fShipAxisInput - The value read from ShipEngines. 
    //      fMaxAxisVisualizationAngle - The maximum possible angle that this should return
    float GetVisualizationAngleTargetFromShipInput(float fShipAxisInput, float fMaxAxisVisualizationAngle)
    {
        // Gets us a value between 0 and 2
        float fInputPercentage = fShipAxisInput + 1;

        // Get the percentage that we are to 2 with our adjusted value
        fInputPercentage = fInputPercentage / 2;

        // Lerp the current pitch input
        return Mathf.LerpAngle(-fMaxAxisVisualizationAngle, fMaxAxisVisualizationAngle, fInputPercentage);
    }

    /// <summary>
    /// Lerp the control stick base to the desired targetRotation slowly over time
    /// </summary>
    /// <param name="targetRotation"></param>
    private void RotateControlStickBaseToTarget(Vector3 targetRotation)
    {
        Quaternion targetQuaternion = Quaternion.identity;
        targetQuaternion.eulerAngles = targetRotation;

        // Lerp to that target with damping
        controlStickBase.transform.localRotation = Quaternion.Lerp(controlStickBase.transform.localRotation, targetQuaternion, Time.deltaTime * fDamping);
    }

    /// <summary>
    /// Lerp the control handle yaw to the desired targetYaw slowly over time
    /// </summary>
    /// <param name="targetYaw"></param>
    private void RotateHandleYawToTarget(float targetYaw)
    {
        // Lerp to the desired yaw target
        handle.transform.localEulerAngles = new Vector3(0.0f, 0.0f, Mathf.LerpAngle(handle.transform.localEulerAngles.z, targetYaw, Time.deltaTime * fDamping));
    }

    /// <summary>
    /// The player is piloting the ship. Control stick animating. Utility tool stowed.
    /// </summary>
    public void UpdatePlayerPilotinglVisualization()
    {
        float fRollTarget, fPitchTarget, fYawTarget;
        Vector3 controlStickBaseTarget;

        #region Control Stick Base Visualization

        // Roll
        fRollTarget = GetVisualizationAngleTargetFromShipInput(ShipEngines.Instance.GetRollInput(), MAX_CONTROL_STICK_BASE_ROLL);

        // Pitch
        fPitchTarget = GetVisualizationAngleTargetFromShipInput(ShipEngines.Instance.GetPitchInput(), MAX_CONTROL_STICK_BASE_PITCH) * -1; // Needs to be inverted due to inverted input

        // Get the target rotation
        controlStickBaseTarget = new Vector3(fControlStickBasePitchOffset + fPitchTarget, fRollTarget, 0);

        // Move the control stick base to the target position
        RotateControlStickBaseToTarget(controlStickBaseTarget);

        #endregion

        #region Control Handle Visualization

        // Yaw
        fYawTarget = GetVisualizationAngleTargetFromShipInput(ShipEngines.Instance.GetYawInput(), MAX_CONTROL_STICK_HANDLE_YAW);

        // Apply the yaw visualization by lerping between the current handle rotation and the desired rotation
        RotateHandleYawToTarget(fYawTarget);
        #endregion
    }

    /// <summary>
    /// Control stick returns to center.
    /// </summary>
    public void UpdateDockingVisualization()
    {
        RotateControlStickBaseToTarget(Vector3.zero);
        RotateHandleYawToTarget(0.0f);
    }
}
