﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipArm : Singleton<ShipArm>
{
    protected ShipArm() { } // guarentee this will always be a singleton only - can't use the constructor!

    public GameObject _clawObject;

    public Transform _clawTargetPosition;
    public Transform _clawStowedPosition;
    public Transform _clawInitialPosition;

    private Vector3 _stowStartPosition; // The position that the claw target started at when moving to the stow arm state

    public const float ARM_TRANSITION_TIME = 1.0F;
    Timer _transitionTimer;

    #region Claw Opening and Closing
    public enum ClawState
    {
        Open = 0,
        Closed,
    }
    private ClawState _currentClawState;

    private Animator _clawAnimator;
    
    Timer _clawSpamTimer;
    private const float CLAW_STATE_SPAM_FREQUENCY = 1.0f;
    #endregion

    private void Start()
    {
        // Initialize our timers
        _transitionTimer = new Timer();
        _clawSpamTimer = new Timer();

        // Get our animator component
        _clawAnimator = _clawObject.GetComponent<Animator>();
    }

    #region Player Controlled Arm Values

    // The minimum and maximum values that the claw target can be moved to. Local position coordinates relative to the ship.
    private const float MAX_X_ARM_MOVEMENT_LOCAL_POSITION = 1.88f; 
    private const float MIN_X_ARM_MOVEMENT_LOCAL_POSITION = -1.7f;
    private const float MAX_Y_ARM_MOVEMENT_LOCAL_POSITION = 2.18f;
    private const float MIN_Y_ARM_MOVEMENT_LOCAL_POSITION = 0.0f;

    // The minimum and maximum values that the controller input will be clamed to. Makes the player need to move the controller less to move the claw to the edge of the area.
    private const float MAX_X_ARM_INPUT = 45f;
    private const float MAX_Y_ARM_INPUT = 45f;
    private const float MAX_Z_ARM_INPUT = 180f;

    // The speed at which the claw will roll based on player input
    private const float CLAW_ROLL_DAMPENING_SPEED = 0.75f;
    private float _clawRollVelocity;
    #endregion

    public void DisableArm()
    {
        // Move the claw object itself to the disabled position
        _clawObject.transform.position = _clawStowedPosition.position;
        _clawObject.transform.rotation = _clawStowedPosition.rotation;

        // Move the claw target position to the disabled position
        _clawTargetPosition.position = _clawStowedPosition.position;
        _clawTargetPosition.rotation = _clawStowedPosition.rotation;

        // Disable the claw gameobject
        _clawObject.SetActive(false);
    }

    /// <summary>
    /// Resets the arm target and actual arm game object to the stowed position.
    /// </summary>
    /// <param name="setArmActive">If set to true, the arm game object will be set to active. If set to false, the arm game object will be set to be inactive.</param>
    private void ReturnArmToStowedPosition(bool setArmActive)
    {
        // Move the claw object to the stowed position
        _clawObject.transform.position = _clawStowedPosition.position;
        _clawObject.transform.rotation = _clawStowedPosition.rotation;

        // Move the claw target to the stowed position
        _clawTargetPosition.transform.position = _clawStowedPosition.position;
        _clawTargetPosition.transform.rotation = _clawStowedPosition.rotation;
        
        // Enable/disable the claw game object
        _clawObject.SetActive(setArmActive);
    }

    /// <summary>
    /// Handles enabling and moving the claw from it's stowed position to it's initial position
    /// </summary>
    /// <returns>True when finished moving the arm to it's initial position</returns>
    public bool UpdateArmToInitialPosition()
    {
        // Initial logic when starting the transition
        if(!_transitionTimer.IsStarted())
        {
            Debug.Log("Beginning to move arm to initial position.");

            // Prepare the claw arm and claw arm target by moving them to the stowed position. Enable the claw arm.
            ReturnArmToStowedPosition(true);

            // Start the timer
            _transitionTimer.StartTimer(ARM_TRANSITION_TIME);
        }

        // Get the percentage of the timer that is completed, and use that to lerp the arm to the target position
        float lerpPercentage = _transitionTimer.GetTimerCompletionPercentage();
        
        // Lerp the claw to the target position
        _clawTargetPosition.position = Vector3.Lerp(_clawStowedPosition.position, _clawInitialPosition.position, lerpPercentage);

        // If the timer has completed, reset it and return true that we've finished moving the arm to it's initial position
        if(lerpPercentage == 1.0)
        {
            _transitionTimer.ResetTimer();
            return true;
        }

        // Need to continue to wait for the claw to reach it's initial position
        return false;
    }

    /// <summary>
    /// Handles moving the claw from it's current position to it's stowed position. Disables the claw when finished moving it to the stowed position
    /// </summary>
    /// <returns>True when the claw has completed being stowed</returns>
    public bool UpdateArmToStowedPosition()
    {
        // Initial logic when starting the transition
        if (!_transitionTimer.IsStarted())
        {
            Debug.Log("Beginning to move arm to stowed position.");

            // Cache off the position that the arm started this transition at
            _stowStartPosition = _clawTargetPosition.position;

            // Start the timer
            _transitionTimer.StartTimer(ARM_TRANSITION_TIME);
        }

        // Get the percentage of the timer that is completed, and use that to lerp the arm to the stowed position
        float lerpPercentage = _transitionTimer.GetTimerCompletionPercentage();

        // Lerp the claw to the stowed position
        _clawTargetPosition.position = Vector3.Lerp(_stowStartPosition, _clawStowedPosition.position, lerpPercentage);

        // If the timer has completed, reset it and return true that we've finished moving the arm to it's stowed position
        if (lerpPercentage == 1.0)
        {
            // Reset our transition timer
            _transitionTimer.ResetTimer();

            // Make sure that both the claw arm and claw target are exactly at the stowed position. Disable the claw arm.
            ReturnArmToStowedPosition(false);

            return true;
        }

        // Need to continue to wait for the claw to reach it's stowed position
        return false;
    }
    
    private void UpdateClawPositionInput()
    {
        Vector3 newTargetPosition = new Vector3();
        
        // Read in the x and y and z rotation of the controller.
        float xInput = InputManager.Instance.GetControllerOrientation().x;
        float yInput = InputManager.Instance.GetControllerOrientation().y;

        // Clamp the input values and normalize them.
        xInput = InputManager.Instance.ClampGVRRawInputValue(xInput, MAX_X_ARM_INPUT) / MAX_X_ARM_INPUT;
        yInput = InputManager.Instance.ClampGVRRawInputValue(yInput, MAX_Y_ARM_INPUT) / MAX_Y_ARM_INPUT;

        // The x input needs to be flipped. This is because as we point the controller up, the value read in from the input manager is negative.
        // Pointing the controller down, the value read in is positive.
        xInput *= -1;

        // Because we're using the x and y input values to lerp our target position, it needs to be a value between 0 and 1.
        // Currently, these values are between -1 and 1.
        // To get these to be between 0 and 1, add 1 and then normalize the resulting value by 2.
        xInput = (xInput + 1) / 2;
        yInput = (yInput + 1) / 2;

        // Take the clamped x and y rotation and apply it to the position of the claw.
        // Note here that the yInput value affects the x component of the target position, and the xInput value affects the y component of the target position.
        // This is because as we rotate the controller left and right (yInput), we want the x position of the claw to be adjusted acordingly.
        // Conversely, as we point the controller up and down (xInput), we want the y position of the claw to be affected.
        newTargetPosition.x = Mathf.Lerp(MIN_X_ARM_MOVEMENT_LOCAL_POSITION, MAX_X_ARM_MOVEMENT_LOCAL_POSITION, yInput);
        newTargetPosition.y = Mathf.Lerp(MIN_Y_ARM_MOVEMENT_LOCAL_POSITION, MAX_Y_ARM_MOVEMENT_LOCAL_POSITION, xInput);

        // If the claw is hitting anything, move it towards the camera
        newTargetPosition.z = _clawTargetPosition.localPosition.z;

        // Set the target position to our new calculated target
        _clawTargetPosition.localPosition = newTargetPosition;
    }

    private void UpdateClawRotationInput()
    {
        Vector3 newClawEulerAngles = new Vector3();

        // Read in the z rotation (roll) of the controller
        float zInput = InputManager.Instance.GetControllerOrientation().z;

        // Calculate the new claw rotation by dampening our input value
        newClawEulerAngles.z = Mathf.SmoothDampAngle(_clawObject.transform.localEulerAngles.z, zInput, ref _clawRollVelocity, CLAW_ROLL_DAMPENING_SPEED);
        newClawEulerAngles.x = _clawObject.transform.localEulerAngles.x; // Want to keep the x rotation of the claw. Authored in the editor.

        // Apply the new claw rotation to the claw
        _clawObject.transform.localEulerAngles = newClawEulerAngles;
    }

    public void SetClawState(ClawState newClawState)
    {
        if(_currentClawState != newClawState)
        {
            Debug.Log("Claw animation change detected. Claw is going to the state " + _currentClawState.ToString());
            
            // Move to the new state
            _currentClawState = newClawState;
            
            // Set the bool that the animation controller looks at to determine if it should be playing the open or close animation
            _clawAnimator.SetBool("ClawClosed", (newClawState == ClawState.Closed));

            // Start our spawm timer to ensure that players are not causing the claw state to change too frequently
            _clawSpamTimer.StartTimer(CLAW_STATE_SPAM_FREQUENCY);
        }
    }

    /// <summary>
    /// Determines if the claw state is being spawmmed. This is used to prevent players from rapidly spamming the claw input, which could break things with the puzzles and the claw animation itself.
    /// </summary>
    /// <returns>True if we should not change the claw gripping state, as it has been too soon since the claw changed states</returns>
    private bool IsClawGrippingBeingSpammed()
    {
        // If we have not started a timer, then we cannot be spamming the input
        if(!_clawSpamTimer.IsStarted())
        {
            return false;
        }

        // If the timer has started, but it has been over our allotted spam time, we are not spamming
        if(_clawSpamTimer.IsExpired())
        {
            return false;
        }

        // The timer has started, and it's still too soon to trigger a claw state switch
        return true;
    }

    private void UpdateClawGripping()
    {
        // If the player presses the action button, toggle the claw state
        if(InputManager.Instance.GetControllerClickButtonDown() && !IsClawGrippingBeingSpammed())
        {
            // If our current claw state is closed, go to the open state. If the current claw state is not closed and therefore open, go to closed.
            ClawState newClawState = _currentClawState == ClawState.Closed ? ClawState.Open : ClawState.Closed;
            SetClawState(newClawState);
        }
    }

    public void UpdatePlayerControl()
    {
        UpdateClawPositionInput();
        UpdateClawRotationInput();
        UpdateClawGripping();
    }

    #region External Arm Helpers
    public bool IsClawAboveCollider(Collider thisCollider)
    {
        RaycastHit hit;

        // Raycast from the arm's position in the direction that the ship is facing. 
        Physics.Raycast(_clawObject.transform.position, ShipManager.Instance._myTransform.forward, out hit, 5.0f);

        // Return whether or not the collider our raycast hit was the desired collider
        return hit.collider == thisCollider;
    }


    #endregion
}
