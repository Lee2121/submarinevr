﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : Singleton<SoundManager>
{
    protected SoundManager() { } // guarentee this will always be a singleton only - can't use the constructor!

    [SerializeField]
    private AudioClip[] oneShotAudioClips;

    [SerializeField]
    private AudioClip[] ambientAudioClips;

    [SerializeField]
    private AudioSource oneShotSource;

    [SerializeField]
    private AudioSource ambientSource;

    [SerializeField]
    private AudioSource speedBuffetSource;

    private AudioClip nullAudioClip;

    public enum Ambient
    {
        Invalid = -1,
        Underwater
    }

    public enum OneShot
    {
        Invalid = -1,
        Sonar_Ping
    }

    #region Speed Buffeting
    [SerializeField]
    private float fMaxSpeedBuffetingPitch;

    [SerializeField]
    private float fMaxSpeedBuffetingSpeed; // At this speed, the pitch of the speedBuffetSource will be equal to fMaxSpeedBuffetingPitch

    #endregion

    private AudioClip GetAmbientAudioClipFromType(Ambient ambient)
    {
        switch(ambient)
        {
            case Ambient.Underwater:        return ambientAudioClips[0];
            default:                        Debug.LogError("GetAmbientAudioClipFromType - Unable to find a valid audio clip for OneShot " + ambientAudioClips.ToString()); return nullAudioClip;
        }
    }

    private AudioClip GetOneShotClipFromType(OneShot oneShot)
    {
        switch(oneShot)
        {
            case OneShot.Sonar_Ping:    return oneShotAudioClips[0];
            default:                    Debug.LogError("GetOneShotClipFromType - Unable to find a valid audio clip for OneShot " + oneShot.ToString());     return nullAudioClip;
        }
    }

    public void PlayAmbientSound(Ambient ambient)
    {
        if(ambientSource.isPlaying)
        {
            Debug.Log("PlayAmbientSound - Stopping clip " + ambientSource.name + " to play ambient sound " + ambient.ToString());
            ambientSource.Stop();
        }

        AudioClip ambientClip = GetAmbientAudioClipFromType(ambient);

        ambientSource.clip = ambientClip;
        ambientSource.Play();
    }

    public void PlayOneShot(OneShot oneShot)
    {
  
        AudioClip oneShotClip = GetOneShotClipFromType(oneShot);

        Debug.Log("PlayOneShot - Playing one shot " + oneShot.ToString());
        oneShotSource.PlayOneShot(oneShotClip);
    }

    private void Start()
    {
        // Initialize the null audio clip
        nullAudioClip = AudioClip.Create("invalid_audio_clip", 1, 1, 1000, false);

        // Start the underwater sound
        PlayAmbientSound(Ambient.Underwater);
    }

    private void UpdateSpeedBuffeting()
    {
        //Debug.Log("curr speed: " + ShipEngines.Instance._currShipSpeed + " -- max buffet speed: " + fMaxSpeedBuffetingSpeed);
        speedBuffetSource.pitch = Mathf.Lerp(0, fMaxSpeedBuffetingPitch, ShipEngines.Instance._currShipSpeed / fMaxSpeedBuffetingSpeed);
    }

    private void Update()
    {
        UpdateSpeedBuffeting();
    }
}
