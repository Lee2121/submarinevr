﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablePuzzleObject : MonoBehaviour
{
    private Collider _myCollider;
    private Material _myMaterial;

    private Timer _myTimer;

    public const float MAX_HIGHLIGHT_WIDTH = 0.10f;
    public const float HIGHLIGHT_EXPAND_TIME = 0.25f;

    private bool _ClawAboveObject;

    public void Start()
    {
        _myCollider = GetComponent<Collider>();
        _myMaterial = GetComponent<Renderer>().material;
        _myTimer = new Timer();
    }

    public void Update()
    {
        if(Input.GetKey(KeyCode.G))
        {
            _myTimer.StartTimer(10.0f);
        }

        if(Input.GetKey(KeyCode.F))
        {
            _myTimer.Invert();
        }

        //Debug.Log("time: " + _myTimer.GetTimerCompletionPercentage());
    }

    // Update function that can be overridden
    public virtual void UpdateInteractablePuzzleObjectCustom() { }

    private void RestartHighlightLerpTimer()
    {
        _myTimer.StartTimer(HIGHLIGHT_EXPAND_TIME);
    }

    public void UpdateInteractablePuzzleObject()
    {
        float currWidth;

        // If the player's claw is above this object, then we should highlight the object
        if (ShipArm.Instance.IsClawAboveCollider(_myCollider))
        {
            // If this is the first time the claw has been above the collider, flip the timer
            if(!_ClawAboveObject)
            {
                RestartHighlightLerpTimer();
            }

            // Lerp the highlight width up
            currWidth = Mathf.SmoothStep(_myMaterial.GetFloat("_FirstOutlineWidth"), MAX_HIGHLIGHT_WIDTH, _myTimer.GetTimerCompletionPercentage());

            // Flag that the claw is above the puzzle object
            _ClawAboveObject = true;
        }
        else
        {
            // If this is the first time the claw has been above the collider, flip the timer
            if (_ClawAboveObject)
            {
                RestartHighlightLerpTimer();
            }

            // Lerp the highlight width down
            currWidth = Mathf.SmoothStep(_myMaterial.GetFloat("_FirstOutlineWidth"), 0.0f, _myTimer.GetTimerCompletionPercentage());

            // Flag that the claw is no longer above the puzzle object
            _ClawAboveObject = false;
        }

        // Apply the calculated width
        _myMaterial.SetFloat("_FirstOutlineWidth", currWidth);
    }

}
