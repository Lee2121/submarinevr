﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
    private float _StartTime = -1;
    private float _ExpireTime = -1;

    /// <summary>
    /// Returns true if the timer has been started
    /// </summary>
    /// <returns></returns>
    public bool IsStarted()
    {
        return (_ExpireTime != -1);
    }

    /// <summary>
    /// Returns true if the timer has finished
    /// </summary>
    /// <returns></returns>
    public bool IsExpired()
    {
        if(!IsStarted())
        {
            Debug.Assert(IsStarted(), "Unstarted timer is being queried for having finished!");
            return false;
        }

        return Time.time >= _ExpireTime;
    }

    /// <summary>
    /// Gets the length of time that the timer will count for
    /// </summary>
    /// <returns></returns>
    public float GetDuration()
    {
        return _ExpireTime - _StartTime;
    }

    /// <summary>
    /// Gets the time in seconds that this counter has been counting for
    /// </summary>
    /// <returns></returns>
    public float GetCompletedDuration()
    {
        return Time.time - _StartTime;
    }

    /// <summary>
    /// Safe version of GetCompletedDuration().
    /// Gets the time in seconds that this counter has been counting for.
    /// Does various validity checks first.
    /// </summary>
    /// <returns></returns>
    public float GetCompletedDurationSafe()
    {
        if (!IsStarted())
        {
            Debug.Assert(IsStarted(), "Unstarted timer is being queried for it's complted duration!");
            return -1;
        }

        // If the timer expired, just return the entire duration
        if (IsExpired())
        {
            return GetDuration();
        }

        // Calculate the time this timer has been counting for. Subtract the time remaining from the total duration of the timer
        return GetCompletedDuration();
    }

    /// <summary>
    /// Returns the completion percentage of the timer (from 0.0 to 1.0)
    /// </summary>
    /// <returns>A value between 0.0 and 1.0, which reflects the completion percentage</returns>
    public float GetTimerCompletionPercentage()
    {
        if (!IsStarted())
        {
            Debug.Assert(IsStarted(), "Unstarted timer is being queried for it's current completion percentage!");
            return -1;
        }

        // If the timer expired, just return 1
        if(IsExpired())
        {
            return 1.0f;
        }

        // Calculate the completed percentage
        return GetCompletedDuration() / GetDuration();
    }

    /// <summary>
    /// Returns a smoothstepped completion percentage of the timer (from 0.0 to 1.0)
    /// </summary>
    /// <returns>A value between 0.0 and 1.0, which reflects a smoothstepped completion percentage</returns>
    public float GetSmoothSteppedCompletionPercentage()
    {
        // Get the current completion percentage
        float t = this.GetTimerCompletionPercentage();

        // If t is invalid, just return it
        if(t == -1)
        {
            Debug.LogError("Was not able to find a valid completion percentage");
            return t;
        }

        return Mathf.SmoothStep(0.0f, 1.0f, t);
    }

    /// <summary>
    /// Starts the timer so that it's expire time is delaySeconds (seconds) from when it was first called
    /// </summary>
    /// <param name="delaySeconds">The time in seconds that you want this function to wait for before returning true.</param>
    public void StartTimer(float delaySeconds)
    {
        _StartTime = Time.time;
        _ExpireTime = _StartTime + delaySeconds;
    }

    /// <summary>
    /// Flips the timer so that however many seconds it has completed, will now be the number of seconds the timer must wait before expiring.
    /// For example, if we had a timer that was set to expire after 10 seconds, and it had counted for 7 seconds when we called Invert(), the timer would need to wait 7 additional seconds.
    /// This is useful in situations where the timer is being used to lerp values, where it could be desired that the lerp reverses and goes in the other direction for the same amount of time that it had previously been lerped.
    /// </summary>
    public void Invert()
    {
        //need to fix 

        if (!IsStarted())
        {
            Debug.Assert(IsStarted(), "Unstarted timer is attempting to invert!");
            return;
        }

        if (IsExpired())
        {
            Debug.Assert(IsExpired(), "Expired timer is attempting to invert!");
        }

        // Get the number of seconds this timer has been counting for
        float countedSeconds = GetCompletedDuration();

        // Set the timer to count for that many seconds again
        StartTimer(countedSeconds);
    }

    /// <summary>
    /// Returns true once there has been delaySeconds since the first time this function was called. Continue calling this until it returns true.
    /// </summary>
    /// <param name="delaySeconds">The time in seconds that you want this function to wait for before returning true.</param>
    /// <param name="resetWhenDone">If true, will cause the timer to set it's expire time back to an invalid value so that it can be reused.</param>
    /// <returns></returns>
    public bool UpdateTimer(float delaySeconds, bool resetWhenDone)
    {
        // Start the timer if it has not yet started
        if(!IsStarted())
        {
            StartTimer(delaySeconds);
            return false;
        }

        // If the current time is greater than our set expire time, we have finished
        if(IsExpired())
        {
            if(resetWhenDone)
            {
                ResetTimer();
            }

            return true;
        }

        return false;
    }

    /// <summary>
    /// Resets the timer so that it can be restarted the next time it is called
    /// </summary>
    public void ResetTimer()
    {
        _ExpireTime = -1;
    }
}
