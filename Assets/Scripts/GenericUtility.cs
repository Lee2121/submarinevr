﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GenericUtility
{
    /// <summary>
    /// Gets the Y value of the coordinate passed in, where the coordiante intersects with the ground.
    /// </summary>
    /// <param name="vCoord"></param>
    /// <returns></returns>
	public static float GetGroundYAtCoord(Vector3 vCoord)
    {
        
        RaycastHit hit;
        Vector3 scanCoord;
        float scanHeightOffset = 5.0f;

        // If the scan has the same y value as the terrain at the specified position, we'll get a false negative as the ray is not able to collide with it's origin
        scanCoord = vCoord;
        scanCoord.y -= scanHeightOffset;

        // Raycast above the given coord
        if (Physics.Raycast(scanCoord, Vector3.up, out hit, Mathf.Infinity))
        {
            return hit.point.y;
        }

        scanCoord = vCoord;
        scanCoord.y += scanHeightOffset;

        // If above didn't work, check below the given point
        if(Physics.Raycast(vCoord, Vector3.down, out hit, Mathf.Infinity))
        {
            return hit.point.y;
        }

        // We didn't find a valid point
        Debug.LogWarning("Unable to find a valid Y coord for point " + vCoord);
        return vCoord.y;
    }
}
