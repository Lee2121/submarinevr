﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public bool GetControllerAppButtonDown()
    {
        return GvrControllerInput.AppButtonDown;
    }

    public bool GetControllerClickButton()
    {
        return GvrControllerInput.ClickButton;
    }

    public bool GetControllerClickButtonDown()
    {
        return GvrControllerInput.ClickButtonDown;
    }

    public Vector3 GetControllerTouchPos()
    {
        return GvrControllerInput.TouchPos;
    }

    public Vector3 GetControllerOrientation()
    {
        return GvrControllerInput.Orientation.eulerAngles;
    }

    // GVR has this weird thing where 360 (or 0) is level. We want to convert this into a value that is useable for determining how much force needs to be applied to our ship to steer.
    // This returns a value between -fMaxValue and fMaxValue
    public float ClampGVRRawInputValue(float fRawInput, float fMaxValue)
    {

        // A value of 360 is level. Clamp the input value so it is between 0 and fMaxValue, or 360 - fMaxValue.
        if (fRawInput < 180)
        {
            if (fRawInput > fMaxValue)
            {
                // Clamp this value to be within bounds. No further adjustment needed
                return fMaxValue;
            }

            // This is a desireable value. Return as is.
            return fRawInput;
        }
        else
        {
            if (fRawInput < 360 - fMaxValue)
            {
                // Clamp this value to be within bounds
                fRawInput = 360 - fMaxValue;
            }

            // Since this value is between 360 and 360 - fMaxValue, the force applied will be based on how far away the input value is from 360
            fRawInput = 360 - fRawInput;

            // Needs to add force in the opposite direction
            fRawInput *= -1;

            return fRawInput;
        }
    }
}
