﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals : Singleton<Globals>
{

    protected Globals() { } // guarentee this will always be a singleton only - can't use the constructor!

    // Global variables
    public GameObject PlayerShip { get { return gPlayerShip; } private set { gPlayerShip = value; } }
    [SerializeField]
    public GameObject gPlayerShip;

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }
}
