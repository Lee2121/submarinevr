﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingDoor : MonoBehaviour
{
    [SerializeField]
    private GameObject _doorLeft;

    [SerializeField]
    private GameObject _doorRight;

    [SerializeField]
    private float _transitionDuration = 3.0f; // How long it takes for the doors open/close

    [SerializeField]
    private float _transitionDist = 12.0f; // How far the doors move from their starting position

    // Starting positions of the doors
    private Vector3 _doorRightStartPos;
    private Vector3 _doorLeftStartPos;

    // Tracks how long the door has been opening
    Timer _transitionTimer;

    private enum DoorState
    {
        Closed,
        Closing,
        Opening, 
        Open
    }

    private DoorState _doorState;

	// Use this for initialization
	void Start ()
    {
        // TODO: Make the player play a short minigame to open the door
        _doorState = DoorState.Opening;

        // Cache off the starting positions of the doors
        _doorRightStartPos = _doorRight.transform.position;
        _doorLeftStartPos = _doorLeft.transform.position;

        // Initialize the timer
        _transitionTimer = new Timer();
	}
	
    /// <summary>
    /// Handles moving the door to the specified state
    /// </summary>
    /// <param name="eState">The state to move the door to</param>
    private void GoToDoorState(DoorState eState)
    {
        if(_doorState != eState)
        {
            Debug.Log("Door is moving from state " + _doorState + " to " + eState);
            _doorState = eState;
        }
    }

    private void UpdateOpeningDoor()
    {
        // Start the transition timer if it has not already started
        if (!_transitionTimer.IsStarted())
        {
            _transitionTimer.StartTimer(_transitionDuration);
        }

        // Calculate the transition target positions. Forward because of how the door was set up in game.
        Vector3 doorLeftTargetPos = _doorLeftStartPos - (_doorLeft.transform.forward * _transitionDist);
        Vector3 doorRightTargetPos = _doorRightStartPos + (_doorRight.transform.forward * _transitionDist);

        // Get the smoothstepped duration from the timer
        float t = _transitionTimer.GetSmoothSteppedCompletionPercentage();

        // Lerp the doors over
        _doorLeft.transform.position = Vector3.Lerp(_doorLeftStartPos, doorLeftTargetPos, t);
        _doorRight.transform.position = Vector3.Lerp(_doorRightStartPos, doorRightTargetPos, t);

        // Once the timer is done, move to the opened state
        if(_transitionTimer.IsExpired())
        {
            GoToDoorState(DoorState.Open);
        }
    }

	// Update is called once per frame
	void Update ()
    {

        switch (_doorState)
        {
            case DoorState.Closed:
                // nothing yet
                break;

            case DoorState.Closing:
                // Nothing yet
                break;

            case DoorState.Open:
                // Nothing yet
                break;

            case DoorState.Opening:

                UpdateOpeningDoor();
                break;
        }
    }
}
